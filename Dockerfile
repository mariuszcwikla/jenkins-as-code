FROM jenkins/jenkins:2.332.1-lts-jdk11
USER root
RUN apt-get update && apt-get install -y lsb-release
# libglib is required for chrome webdriver (selenium)
# cutting the corners here, hoping that chromium-driver will fetch all required dependencies ;)
RUN apt-get install -y libglib2.0-0 libnss3 chromium-driver
RUN curl -fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
  https://download.docker.com/linux/debian/gpg
RUN echo "deb [arch=$(dpkg --print-architecture) \
  signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
  https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
RUN apt-get update && apt-get install -y docker-ce-cli

USER jenkins
RUN jenkins-plugin-cli --plugins pipeline-maven config-file-provider configuration-as-code workflow-aggregator \
	timestamper ws-cleanup pipeline-stage-view git cloudbees-folder sshd \
	pipeline-model-definition job-dsl pipeline-utility-steps dependency-check-jenkins-plugin:5.1.2

# New plugins added as separate steps to speed up builds (utilizing build cache)
RUN jenkins-plugin-cli --plugins docker-plugin:1.2.7 docker-workflow:1.28
# warnings-ng for trivy reports
RUN jenkins-plugin-cli --plugins warnings-ng:9.11.1
COPY jenkins.yml /var/jenkins_home/casc_configs/jenkins.yaml
COPY pipelines/* /var/jenkins_home/pipelines/
ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc_configs/jenkins.yaml