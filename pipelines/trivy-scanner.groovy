pipeline {
  agent {
      docker {
          image 'bitnami/trivy:latest'
          args  """-v /var/run/docker.sock:/var/run/docker.sock --entrypoint=''"""
      }
  }
  stages{
      stage('Trivy version') { 
          steps{
              sh "trivy help"
          }
      }
      stage('Trivy test') { 
          steps {
              sh "trivy --cache-dir=.cache/trivy image -f json -o results.json vulnerables/web-dvwa"
              recordIssues(tools: [trivy(pattern: 'results.json')])
          }
      }
  }
}